scope = {"global": {"parent": False, "vars": []}}


def create(nsp, pnt):
    scope[nsp] = {"parent": pnt, "vars": []}


def add(nsp, var):
    n = scope[nsp]
    v = n["vars"]
    v.append(var)
    n["vars"] = v
    scope[nsp] = n


def get(nsp, var):
    if var in scope[nsp]["vars"]:
        print(nsp)
    else:
        prn = scope[nsp]["parent"]
        while True:
            if not prn:
                print("None")
                break
            if var in scope[prn]["vars"]:
                print(prn)
                break
            else:
                prn = scope[prn]["parent"]


comm = [input().split() for i in range(int(input()))]
for com in comm:
    if com[0] == "add":
        add(com[1], com[2])
    elif com[0] == "create":
        create(com[1], com[2])
    elif com[0] == "get":
        get(com[1], com[2])
