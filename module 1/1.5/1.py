class MoneyBox:
    money = 0

    def __init__(self, capacity):
        self.capacity = capacity

    def can_add(self, v):
        if self.money + v <= self.capacity:
            return True
        else:
            return False

    def add(self, v):
        self.money += v
