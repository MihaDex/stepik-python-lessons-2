import time


class Loggable:
    def log(self, msg):
        print(str(time.ctime()) + ": " + str(msg))


class LoggableList(Loggable, list):
    def append(self, item):
        self.log(item)
        return super().append(item)


a = LoggableList([1, 2, 3])
a.append(4)
print(a)
