cls = [input().split() for i in range(int(input()))]
com = [input().split() for i in range(int(input()))]
rel = {}


def search(prnt, chld):
    if chld not in rel:
        return False
    elif prnt in rel[chld]:
        return True
    elif len(rel[chld]) == 0:
        return False
    else:
        trigger = False
        for i in rel[chld]:
            if search(prnt, i):
                trigger = True
        if trigger:
            return True
        else:
            return False


for item in cls:
    if len(item) > 1:
        rel[item[0]] = item[2: len(item)]
    else:
        rel[item[0]] = []
for c in com:
    if c[0] == c[1]:
        print('Yes')
    elif c[1] in rel:
        if c[0] in rel[c[1]]:
            print('Yes')
        else:
            if search(c[0], c[1]):
                print('Yes')
            else:
                print('No')
    else:
        print('No')
