objects = [1, 2, 3, "rrf", True, "true", "True"]
res = []
for o in objects:
    if len(res) == 0:
        res.append(o)
    else:
        flag = True
        for i in res:
            if i is o:
                flag = False
        if flag:
            res.append(o)

print(len(res))
